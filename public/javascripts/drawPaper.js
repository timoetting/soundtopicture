const drawSpectrum = function(data) {
  // Get a reference to the canvas object

  // data = [[130,50,0], [0,90,130]]
  var canvas = document.getElementById('draw-canvas');
  // Create an empty project and a view for the canvas:
  paper.setup(canvas);
  // Create a Paper.js Path to draw a line into it:
  var path = new paper.Path();
  let raster = new paper.Raster('poster-template', paper.view.center)
  raster.on('load', function() {
    const rows = data.length
    const cols = Math.floor(data[0].length * (16/22))
    for (var y = 0; y < rows; y++) {
      for (var x = 0; x < cols; x++) {
        // console.log(x, y)
        var intensity = data[y][x]
        raster.setPixel(x,y, new paper.Color(1,0,0,intensity/130) )
      }
      console.log('gezeichnet: ', y/rows)
    }
    // paper.view.viewSize = new paper.Size(900,600)
    // raster.setPixel(10,11, new paper.Color(1,0,0,1) )
    // raster.position = paper.view.center
    paper.view.draw();
  });
}