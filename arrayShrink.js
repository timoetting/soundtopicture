const shrink2dArray = function(array, colsCount){
  let shrinkedArray = []
  array.map((row, rowIndex) => {
    const colsPerCol = Math.floor(row.length / colsCount)
    shrinkedArray.push([])
    let sumToAdd = 0
    row.map((col, index) => {
      sumToAdd += col
      if ((index + 1) % colsPerCol === 0){
        shrinkedArray[rowIndex].push(sumToAdd)
        sumToAdd = 0
      }
    })
  })
  return shrinkedArray
}

const transpose2dArray = function (array) {
  let transposedArray = array[0].map( (col, i) => { 
    return array.map(function(row) { 
      return row[i] 
    })
  });
  return transposedArray
}

let startArray = 
  [
    [1,2,3,4,5],
    [6,7,8,9,10],
    [11,12,13,14,15],
    [16,17,18,19,20]
  ]

let myShrinkedArray = shrink2dArray(startArray, 2)
console.log(myShrinkedArray)
myShrinkedArray = transpose2dArray(myShrinkedArray)
console.log(myShrinkedArray)
myShrinkedArray = shrink2dArray(myShrinkedArray, 2)
console.log(myShrinkedArray)

