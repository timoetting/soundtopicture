SpectroHelper = require ('./lib/spectroHelper.js')
SpectroDrawer = require ('./lib/spectroDrawer.js')

const onSpectroProcess = (percent) => {
  console.log('spectro process: ', percent)
}

const onSpectroEnd = (data) => {
  console.log('finish: ', data.length)
  SpectroDrawer.drawSpectrum('testpng_allesneu', data)
}

SpectroHelper.createSpectrum('./conversion/wavs/allesneu.wav', onSpectroProcess, onSpectroEnd)
// SpectroDrawer.pngDraw('test')
