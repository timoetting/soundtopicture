const fs = require('fs'),
Spectro = require('./spectro.js')

module.exports.createSpectrum = function (wavPath, onProcess, callback) {

  /** @type {Stream} A stream with pcm audio data. (The first 44 bytes are the header...) **/
  const audioFile = fs.createReadStream(wavPath, { start: 44 })
  const audioFileSize = fs.statSync(wavPath).size
  // const plotCount = 1657 // window size (1024 by default) * 1.618
  const plotCount = 300
  // const plotCount = 300 // window size (1024 by default) * 1.618

  const plotDistance = Math.floor((audioFileSize - 44) / (plotCount * 2)) //wie sich die dateigröße auf das array innerhalb von spectro auswirkt hängt von der bitzahl ab (32bit = "/2")

  const wFunc = 'Blackman'

  const spectro = new Spectro({
    wFunc: wFunc,
    wSize: 1024,
    plotDistance 
  })

  audioFile.pipe(spectro)

  // Capture when the file stream completed
  let fileRead = false
  audioFile.on('end', () => fileRead = true)

  spectro.on('data', (err, frame) => {
    // Check if any error occured
    if (err) return console.error('Spectro ended with error:', err)
    onProcess((frame.index / plotCount) * 100)
  })
  
  spectro.on('end', (err, data) => {
    // Check if the file was read completely
    if (fileRead !== true) return console.log('Have not finished reading file')
    // Check if any error occured
    if (err) return console.error('Spectro ended with error:', err)
    // Stop spectro from waiting for data and stop all of it's workers
    spectro.stop()
  
    const time = (spectro.getExecutionTime() / 1000) + 's'
    console.log(`Spectrogram created in ${time}`)
  
    const max = Spectro.maxApplitude(data)
    const min = Spectro.minApplitude(data)
    console.log(`Max amplitude is ${max}, min amplitude is ${min}`)
  
    callback(data)
  })
}