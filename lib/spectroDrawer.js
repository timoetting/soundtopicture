// const paper = require('paper');
const paper = require('paper-jsdom-canvas');
const fs = require('fs');
const PNG = require('pngjs').PNG


// module.exports.drawSpectrum = function(filename, data) {
//   let width = data[0].length
//   let height = data.length
//   let canvas = paper.createCanvas(width, height)
//   let sourceCanvas = paper.createCanvas(width, height)
//   paper.setup(canvas);
  
//   let raster = new paper.Raster(sourceCanvas, paper.view.center)
//   raster.on('load', function() {
//     const rows = data.length
//     const cols = Math.floor(data[0].length * (16/22))
//     for (var y = 0; y < rows; y++) {
//       for (var x = 0; x < cols; x++) {
//         // console.log(x, y)
//         var intensity = data[y][x]
//         raster.setPixel(x,y, new paper.Color(0,0,0,intensity/130) )
//       }
//       console.log('gezeichnet: ', y/rows)
//     }
//     paper.view.draw();
//     exportPNG(canvas, filename, () => {console.log('fertig gezeichnet')})
//   });
// }

module.exports.drawSpectrum = function(filename, data) {

  const rows = data.length
  const cols = Math.floor(data[0].length * (16/22))
  var png = new PNG({
    width: cols,
    height: rows,
    filterType: -1,
    colorType: 0
  })
  for (var y = 0; y < png.height; y++) {
    for (var x = 0; x < png.width; x++) {

      var intensity = data[y][x]

      // Draw the pixel
      var idx = (png.width * y + x) << 2
      png.data[idx] = valBetween(255 - Math.floor(255 * (intensity/130)), 0, 255)
      png.data[idx + 1] = valBetween(255 - Math.floor(255 * (intensity/130)), 0, 255)
      png.data[idx + 2] = valBetween(255 - Math.floor(255 * (intensity/130)), 0, 255)
      png.data[idx + 3] = 255
    }
    console.log(`zeichne via pngjs ${y} / ${png.height}`)
  }
  png.pack().pipe(fs.createWriteStream('public/images/visualizations/' + filename + '_grey_pngjs.png'))
  console.log(`Spectrogram written`)

}

const exportPNG = function(canvas, filename, fn) {
  var dst = new PNG({
    width: canvas.width, 
    height: canvas.height,
    colorType: 0
  });
  canvas.pngStream()
  .pipe(new PNG())
  .on('parsed', function() {
      this.bitblt(dst, 0, 0, canvas.width, canvas.height, 0, 0);
      dst.pack().pipe(fs.createWriteStream('public/images/visualizations/' + filename + '_grey.png'));
  });
}

const valBetween = function(v, min, max) {
  return (Math.min(max, Math.max(min, v)));
}