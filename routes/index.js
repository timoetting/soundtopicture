module.exports = function(io) {
  const express = require('express'),
    app = express(),
    expressWs = require('express-ws')(app),
    fs = require('fs'),
    path = require('path'),
    router = express.Router(),
    multer  = require('multer'),
    upload = multer({ dest: 'conversion/uploads/' }),
    ffmpeg = require('fluent-ffmpeg'),
    spectroHelper = require('./../lib/spectroHelper.js');
  // var expressWs = require('express-ws')(app);
  
  /* GET home page. */
  router.get('/', function(req, res, next) {
    console.log('render index') 
    console.log(__dirname)
    res.render('index', { title: 'Express' });
  });
  
  router.get('/convert', function(req, res, next) {
    spectroHelper(`${__dirname}/../conversion/wavs/siriusmo.wav`, (data) => {
      console.log('fertig gespectrt...', data.length)
      res.json(data)
    })
  });

  router.get('/test', function(req, res, next) {
    res.send('hello')
  });
  
  /* GET home page. */
  router.post('/upload', upload.single('afile'), function(req, res, next) {
    const wavFileId = req.file.filename  + '.' + req.file.originalname
    const wavFile = 'conversion/wavs/' + wavFileId + '.wav';
    ffmpeg(req.file.path)
      .format('wav')
      .audioChannels(1)
      .on('error', function(err) {
        res.send(500)
      })
      .on('end', function() {
        res.json({wavFileId})
      })
      .save(wavFile)
  });
  
  io.on('connection', function(socket){
    console.log('a user connected');
    socket.on('disconnect', function(){
      console.log('user disconnected');
    });
    socket.on('startSpectro', function(wavFileId){
      console.log('sarting spectro durch socket message from front end');
      spectroHelper(`${__dirname}/../conversion/wavs/${wavFileId}.wav`, onSpectroHelperProcess, onSpectroHelperEnd);
    })
  });

  const onSpectroHelperProcess = (percent) => {
    console.log('processingSpectro', percent)
    io.emit('processingSpectro', percent);
  }

  const onSpectroHelperEnd = (data) => {
    console.log('fertig gespectrt...', data.length)
    io.emit('finishedSpectro', data);
  }

  return router
}
