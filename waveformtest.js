const paper = require('paper');
const PNG = require('pngjs').PNG
const fs = require('fs');

var waveform = require('waveform-node');
console.log('start')
var options = {
  waveformType: waveform.waveformType.LINE,
  numOfSample: 1000000
};
waveform.getWaveForm(__dirname + '/mp3s/samsmith.mp3', options, function (error, peaks) {
  if (error) {
    console.log(error)
    return;
  }

  // Get peaks
  // console.log(peaks);

  logUsedMemory()
  drawWaveline(peaks)
});

const drawWaveline = function (data) {
  // let width = data.length
  // let width = data.length
  // let height = 300
  let height = 4800
  let width = 3600
  let canvas = paper.createCanvas(width, height)
  const lineDistance = 60
  const lineStart = 300
  const gain = 30
  const vizWidth = 3000
  const vizRows = 60
  const splitData = splitArray(data, Math.floor(data.length / vizRows))

  // Create an empty project and a view for the canvas:
  paper.setup(canvas);

  // splitData.map( (row, rowIndex) => {
  //   let pathUp = new paper.Path();
  //   let pathDown = new paper.Path();
  //   pathUp.fillColor = '#b33947';
  //   pathDown.fillColor = '#b33947';
  //   const start = new paper.Point(0 + 300, rowIndex * gain + rowIndex * lineDistance + lineStart);
  //   pathUp.moveTo(start);
  //   pathDown.moveTo(start);
  //   row.map( (plot, index) => {
  //     pathUp.lineTo(new paper.Point(index + 300, gain * plot + rowIndex * lineDistance + lineStart));
  //     pathDown.lineTo(new paper.Point(index + 300, - gain * plot + rowIndex * lineDistance + lineStart));
  //   })
  //   pathUp.lineTo(new paper.Point(row.length - 1 + 300, rowIndex * lineDistance + lineStart));
  //   pathDown.lineTo(new paper.Point(row.length - 1 + 300, rowIndex * lineDistance + lineStart));
  //   pathUp.lineTo(new paper.Point(0 + 300, rowIndex * lineDistance + lineStart));
  //   pathDown.lineTo(new paper.Point(0 + 300, rowIndex * lineDistance + lineStart));
  // })

  const stepsOnX = vizWidth / splitData[0].length
  splitData.map((row, rowIndex) => {
    let path = new paper.Path();
    path.strokeColor = '#b33947';
    // const start = new paper.Point(0 + 300, rowIndex * gain + rowIndex * lineDistance + lineStart);
    // path.moveTo(start);
    row.map((plot, index) => {
      const currentPosition = new paper.Point(stepsOnX * index + 300, gain * plot + rowIndex * lineDistance + lineStart)
      if (index === 0) {
        path.moveTo(currentPosition);
      }
      path.lineTo(currentPosition);
    })
    // path.lineTo(new paper.Point(row.length - 1 + 300, rowIndex * lineDistance + lineStart));
    // path.lineTo(new paper.Point(0 + 300, rowIndex * lineDistance + lineStart));
  })

  // Draw the view now:
  paper.view.draw();
  exportPNG(canvas, 'test_line_samsmith.png', () => { console.log('fertig gezeichnet') })
  console.log('fertig gezeichnet')
}  

const exportPNG = function (canvas, filename, fn) {
  var dst = new PNG({
    width: canvas.width,
    height: canvas.height,
    // colorType: 0 //Black and white
    colorType: 2 //rgb
  });
  canvas.pngStream()
    .pipe(new PNG())
    .on('parsed', function () {
      this.bitblt(dst, 0, 0, canvas.width, canvas.height, 0, 0);
      dst.pack().pipe(fs.createWriteStream('public/images/visualizations/' + filename + '_grey.png'));
    });
}

const splitArray = function (arr, cols) {

  var newArr = [];
  while (arr.length) newArr.push(arr.splice(0, cols));

  return newArr
}

const logUsedMemory = function () {
  const used = process.memoryUsage().heapUsed / 1024 / 1024;
  console.log(`The script uses approximately ${Math.round(used * 100) / 100} MB`);
}

// drawWaveline()